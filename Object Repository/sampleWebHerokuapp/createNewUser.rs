<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>Endpoint for create new user</description>
   <name>createNewUser</name>
   <tag></tag>
   <elementGuidId>7fe64a58-bc82-4e21-bb2d-25b102c0561a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;age\&quot;: ${age},\n  \&quot;avatar\&quot;: \&quot;${avatar}\&quot;,\n  \&quot;gender\&quot;: \&quot;${gender}\&quot;,\n  \&quot;password\&quot;: \&quot;${password}\&quot;,\n  \&quot;username\&quot;: \&quot;${username}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>a888f01c-b095-4656-ab81-f861b7cc765a</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.baseUrl}/api/users/json</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'QaBootcamp'</defaultValue>
      <description></description>
      <id>7c4fb270-53ef-4e2c-918d-daeaaffc72c9</id>
      <masked>false</masked>
      <name>username</name>
   </variables>
   <variables>
      <defaultValue>'akuntesting123'</defaultValue>
      <description></description>
      <id>2e159bdd-8f4d-4ccb-a6ff-c94354b3d02e</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <variables>
      <defaultValue>'MALE'</defaultValue>
      <description></description>
      <id>34500cae-22b4-4473-8db0-e25490b58552</id>
      <masked>false</masked>
      <name>gender</name>
   </variables>
   <variables>
      <defaultValue>20</defaultValue>
      <description></description>
      <id>f57d6abd-9932-4473-83bf-82732b053c4c</id>
      <masked>false</masked>
      <name>age</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>2b1fcda1-d3ba-4c53-b1db-1783e14d427f</id>
      <masked>false</masked>
      <name>avatar</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

def variables = request.getVariables()
def username = variables.get('username')
def password = variables.get('password')
def gender = variables.get('gender')
def age = variables.get('age')
def avatar = variables.get('avatar')

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

//assertThat(response.getResponseBodyContent().getAt(&quot;id&quot;)).isNotEqualTo(null)
WS.verifyElementPropertyValue(response, 'username', username)
WS.verifyElementPropertyValue(response, 'password', password)
WS.verifyElementPropertyValue(response, 'gender', gender)
WS.verifyElementPropertyValue(response, 'age', age)
WS.verifyElementPropertyValue(response, 'avatar', avatar)
assertThat(response.getStatusCode()).isEqualTo(200)

GlobalVariable.globalId = WS.getElementPropertyValue(response, 'id')

System.out.println(GlobalVariable.globalId)






</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
